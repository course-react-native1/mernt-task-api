import os

from django.core.wsgi import get_wsgi_application

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'merntTaskApi.settings')

app = get_wsgi_application()

#! SERVERLESS CONFIGURATION: 
os.system("python manage.py migrate")
# os.system("python manage.py create_admin_user") 
os.system("python manage.py runserver")